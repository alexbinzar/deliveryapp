package ro.nutrivention.deliveryapp;

import java.util.List;


public class Vehicle {

	private int capacity;
	private double distance;
	private int speed;
	private String status= "parked";
	private String license;
	
	
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	List<Order> orders;
	
	//Constructor for Vehicles
	public Vehicle( int capacity, double distance , int speed) {
		this.capacity=capacity;
		this.distance=distance;
		this.speed=speed;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	/*private static String Limit() {
		
	}*/
}

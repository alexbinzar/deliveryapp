package ro.nutrivention.deliveryapp;

import java.util.ArrayList;
import java.util.List;
import ro.nutrivention.deliveryapp.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

	//lista dos veiculos;
	List<Vehicle> veiculos;
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("getStatus")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt(@PathParam("id") String id) {
        return "GOT IT!";
        //go through lista dos vehicolos
            //find id == vehicolo licence
                //return vehicolo status
    }

    @GET
    @Path("initializelist")
    @Produces(MediaType.TEXT_PLAIN)
    public void init(@PathParam("id") String id) {
       veiculos = new ArrayList<>();

       Vehicle v1 = new Vehicle(2, 3, 4);
       veiculos.add(v1);
       Vehicle v2 = new Vehicle(5, 4, 8);
       veiculos.add(v2);
       Vehicle v3 = new Vehicle(7, 6, 10);
       veiculos.add(v3);
       Vehicle v4 = new Vehicle(9, 8, 12);
       veiculos.add(v4);
       System.out.println(veiculos.size());
    }
}

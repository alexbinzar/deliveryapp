package ro.nutrivention.deliveryapp;

import java.time.LocalDate;

public class Order {

	private LocalDate orderDate;
	private double price;
	private int number_orders;
	
	
	
	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public int getNumber_orders() {
		return number_orders;
	}

	public void setNumber_orders(int number_orders) {
		this.number_orders = number_orders;
	}

	// constructor for Order
	public Order(LocalDate orderDate, double price) {
		this.orderDate= orderDate;
		this.price= price;
	}
	
	// getter for the LocalDate order
	public LocalDate getLocalDate() {
		return orderDate;
	}
	
	// setter for the LocalDate order
	
	public void setLocalDate(LocalDate orderDate) {
		this.orderDate=orderDate;
	}
	
	// getter for the order's price
	public double getPrice() {
		return price;
	}
	
	// setter for the order's price
	public void setPrice(double price) {
		this.price=price;
	}
	
	
	/*public static setNumberOrders() {
		number_orders=0;
		If()
		number_orders++;
	}*/
}

package ro.nutrivention.deliveryapp;

/**
 *
 * @author seva
 *
 */

public class MenuItem {
	
	int numped;
	String name;
	String description;
	double price;
	boolean glutenFree;
	
	
	public void print(){
		System.out.print(numped + " - " + name + "  "
				+ price + " Ron\n" 
				+ "    " + description + " ");
		
		if (glutenFree) {
			System.out.println("Gluten-free!" + "\n");
	} else { 
		System.out.println("" + "\n");
	}
	}
	

}
